# Swedbank Casco Insurance Front-end application 

This project is a front-end ui for ***Casco Insurance application*** based on ***Angular 10***. This application has different menus such as insurance calculator, list of all vehicles of which insurance fee has been calculated, cars and risks.
The project has very minimal UI built based on Angular Material UI.

Clicking on Swedbank logo will navigate to the insurance calculator.

*App compatibility: Google Chrome, Microsoft Edge* 

## Before run
Before running this project, the server application needs to be run based on different environment.

Please refer to [Casco Server Documentation](https://bitbucket.org/vinodrockson/casco) for more details.

## Development environment

Run `npm install` and `ng serve --open` for a dev server opens `http://localhost:4200/` in an internet browser. 

## Production environment

This application had already dockerized and deployed to Amazon Web Services(AWS) via [Bitbucket Pipeline](https://bitbucket.org/vinodrockson/cascoclient/addon/pipelines/home).

Check this [link](http://34.227.192.172) to go production server. 

*If you see 'ngnix 404 page' at any point, please refresh the page again.*



