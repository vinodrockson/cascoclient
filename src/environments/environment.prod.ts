export const environment = {
  DefaultLanguage: 'en',
  production: true,
  development: false,
  environmentName: 'PROD',
  baseURL: 'http://3.80.63.202:8080/',
  itemsPerPage: 10
};
