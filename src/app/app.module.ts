import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {RouterModule, Routes} from '@angular/router';
import {CalculatorComponent} from './calculator/calculator.component';
import {AppRoutingModule} from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {AppInterceptor} from './shared/interceptor/app.interceptor';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatChipsModule} from '@angular/material/chips';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {VehicleComponent} from './vehicle/vehicle.component';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {CarComponent} from './car/car.component';
import {MatIconModule} from '@angular/material/icon';
import {AddCarComponent} from './car/add-car/add-car.component';
import {MatDialogModule} from '@angular/material/dialog';
import {UpdateCarComponent} from './car/update-car/update-car.component';
import {DeleteCarComponent} from './car/delete-car/delete-car.component';
import {RestoreCarComponent} from './car/restore-car/restore-car.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RiskComponent} from './risk/risk.component';
import {AddRiskComponent} from './risk/add-risk/add-risk.component';
import {UpdateRiskComponent} from './risk/update-risk/update-risk.component';
import {DeleteRiskComponent} from './risk/delete-risk/delete-risk.component';
import {RestoreRiskComponent} from './risk/restore-risk/restore-risk.component';
import {MatTabsModule} from '@angular/material/tabs';

const appRoutes: Routes = [
  {
    path: '',
    component: CalculatorComponent
  },
  {
    path: 'vehicles',
    component: VehicleComponent
  },
  {
    path: 'cars',
    component: CarComponent
  },
  {
    path: 'risks',
    component: RiskComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    VehicleComponent,
    CarComponent,
    AddCarComponent,
    UpdateCarComponent,
    DeleteCarComponent,
    RestoreCarComponent,
    RiskComponent,
    AddRiskComponent,
    UpdateRiskComponent,
    DeleteRiskComponent,
    RestoreRiskComponent
  ],
    imports: [
        BrowserModule,
        NgbModule,
        FontAwesomeModule,
        RouterModule.forChild(appRoutes),
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatStepperModule,
        MatFormFieldModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatInputModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        MatChipsModule,
        MatSlideToggleModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatIconModule,
        MatDialogModule,
        MatSnackBarModule,
        MatTabsModule
    ],
  exports: [RouterModule],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AppInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
