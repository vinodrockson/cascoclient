import { TestBed } from '@angular/core/testing';

import { InsuranceFeeService } from './insurance-fee.service';

describe('InsuranceFeeService', () => {
  let service: InsuranceFeeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InsuranceFeeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
