import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {VehicleProducer} from '../model/vehicle-producer';
import {environment} from '../../../environments/environment';
import {VehicleProducerList} from '../model/vehicle-producer-list';

@Injectable({
  providedIn: 'root'
})
export class VehicleProducerService {
  private VEHICLE_PRODUCER_BASE_URL = 'vehicle-producer';

  constructor(private httpClient: HttpClient) {
  }

  public getActiveProducers() {
    return this.httpClient.get<VehicleProducer[]>(this.VEHICLE_PRODUCER_BASE_URL.concat('/active'));
  }

  public getAllProducers(sort: string, order: string, page: number) {
    return this.httpClient.get<VehicleProducerList>(this.VEHICLE_PRODUCER_BASE_URL + '?page=' + page + '&items='
      + environment.itemsPerPage + '&sort=' + sort + '&order=' + order);
  }

  public createProducer(vehicleProducer: VehicleProducer) {
    return this.httpClient.post(this.VEHICLE_PRODUCER_BASE_URL, vehicleProducer);
  }

  public updateProducer(vehicleProducer: VehicleProducer) {
    return this.httpClient.put(this.VEHICLE_PRODUCER_BASE_URL, vehicleProducer);
  }

  public deleteProducer(id: string) {
    return this.httpClient.get(this.VEHICLE_PRODUCER_BASE_URL.concat('/delete/').concat(id));
  }

  public restoreProducer(id: string) {
    return this.httpClient.get(this.VEHICLE_PRODUCER_BASE_URL.concat('/restore/').concat(id));
  }
}
