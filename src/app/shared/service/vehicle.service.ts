import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {VehicleList} from '../model/vehicle-list';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  private VEHICLE_BASE_URL = 'vehicle';

  constructor(private httpClient: HttpClient) {
  }

  public getAllVehicles(sort: string, order: string, page: number) {
    return this.httpClient.get<VehicleList>(this.VEHICLE_BASE_URL + '?page=' + page + '&items=' + environment.itemsPerPage
      + '&sort=' + sort + '&order=' + order);
  }

  public getAllValidVehicleFields(type: string) {
    return this.httpClient.get(this.VEHICLE_BASE_URL.concat('/fields?type=').concat(type));
  }
}
