import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SettingListDto} from '../model/setting-list-dto';
import {Setting} from '../model/setting';

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  private SETTING_BASE_URL = 'setting';

  constructor(private httpClient: HttpClient) {
  }

  public getAllSettingsByType(type: string, sort: string, order: string, page: number) {
    return this.httpClient.get<SettingListDto>(this.SETTING_BASE_URL + '?type=' + type + '&page=' + page + '&items='
      + environment.itemsPerPage + '&sort=' + sort + '&order=' + order);
  }

  public createSetting(setting: Setting) {
    return this.httpClient.post(this.SETTING_BASE_URL, setting);
  }

  public updateSetting(setting: Setting) {
    return this.httpClient.put(this.SETTING_BASE_URL, setting);
  }

  public deleteSetting(id: string) {
    return this.httpClient.get(this.SETTING_BASE_URL.concat('/delete/').concat(id));
  }

  public restoreSetting(id: string) {
    return this.httpClient.get(this.SETTING_BASE_URL.concat('/restore/').concat(id));
  }
}
