import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Vehicle} from '../model/vehicle';
import {InsuranceFee} from '../model/insurance-fee';

@Injectable({
  providedIn: 'root'
})
export class InsuranceFeeService {
  private INSURANCE_FEE_BASE_URL = 'insurance-fee';

  constructor(private httpClient: HttpClient) {
  }

  public postInsuranceFee(vehicle: Vehicle, previousIndemnity: boolean) {
    return this.httpClient.post<InsuranceFee>(this.INSURANCE_FEE_BASE_URL + '?pi=' + previousIndemnity, vehicle);
  }
}
