import { TestBed } from '@angular/core/testing';

import { VehicleProducerService } from './vehicle-producer.service';

describe('VehicleProducerService', () => {
  let service: VehicleProducerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VehicleProducerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
