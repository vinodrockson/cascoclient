export class VehicleProducer {
  id: string;
  name: string;
  averagePurchasePrice: number;
  riskCoefficient: number;
  isActive: boolean;
  createdDate: Date;

  constructor(id: string, name: string, averagePurchasePrice: number, riskCoefficient: number, isActive: boolean, createdDate: Date) {
    this.id = id;
    this.name = name;
    this.averagePurchasePrice = averagePurchasePrice;
    this.riskCoefficient = riskCoefficient;
    this.isActive = isActive;
    this.createdDate = createdDate;
  }
}
