export class Setting {
  id: string;
  settingType: string;
  name: string;
  value: string;
  isActive: boolean;
  createdDate: Date;

  constructor(id: string, settingType: string, name: string, value: string, isActive: boolean, createdDate: Date) {
    this.id = id;
    this.settingType = settingType;
    this.name = name;
    this.value = value;
    this.isActive = isActive;
    this.createdDate = createdDate;
  }
}
