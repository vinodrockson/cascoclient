export class Risk {
  id: string;
  name: string;
  countedForCalculation: boolean;

  constructor(id: string, name: string, countedForCalculation: boolean) {
    this.id = id;
    this.name = name;
    this.countedForCalculation = countedForCalculation;
  }
}
