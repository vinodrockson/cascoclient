import {VehicleProducer} from './vehicle-producer';

export class VehicleProducerList {
  vehicleProducerList: VehicleProducer[];
  currentPage: number;
  totalElements: number;
}
