import {Setting} from './setting';

export class SettingListDto {
  settingList: Setting[];
  currentPage: number;
  totalElements: number;
}
