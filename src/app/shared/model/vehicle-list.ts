import {VehicleDto} from './vehicle-dto';

export class VehicleList {
  vehicleList: VehicleDto[];
  currentPage: number;
  totalElements: number;
}
