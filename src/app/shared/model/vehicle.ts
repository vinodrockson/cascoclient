import {VehicleProducer} from './vehicle-producer';

export class Vehicle {
  id: string;
  plateNumber: string;
  firstRegistrationYear: number;
  purchasePrice: number;
  vehicleProducer: VehicleProducer;
  mileage: bigint;
  previousIndemnity: number;
  isActive: boolean;

  constructor(id: string, plateNumber: string, firstRegistrationYear: number, purchasePrice: number, vehicleProducer: VehicleProducer,
              mileage: bigint, previousIndemnity: number, isActive: boolean) {
    this.id = id;
    this.plateNumber = plateNumber;
    this.firstRegistrationYear = firstRegistrationYear;
    this.purchasePrice = purchasePrice;
    this.vehicleProducer = vehicleProducer;
    this.mileage = mileage;
    this.previousIndemnity = previousIndemnity;
    this.isActive = isActive;
  }
}

