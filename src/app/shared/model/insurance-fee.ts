import {Risk} from './risk';

export class InsuranceFee {
  id: string;
  annualFee: number;
  monthlyFee: number;
  vehicleProducer: string;
  firstRegistrationYear: bigint;
  mileage: bigint;
  previousIndemnity: number;
  feeCalculatedDate: Date;
  risks: Risk[];

  constructor(id: string, annualFee: number, monthlyFee: number, vehicleProducer: string, firstRegistrationYear: bigint, mileage: bigint,
              previousIndemnity: number, feeCalculatedDate: Date, risks: Risk[]) {
    this.id = id;
    this.annualFee = annualFee;
    this.monthlyFee = monthlyFee;
    this.vehicleProducer = vehicleProducer;
    this.firstRegistrationYear = firstRegistrationYear;
    this.mileage = mileage;
    this.previousIndemnity = previousIndemnity;
    this.feeCalculatedDate = feeCalculatedDate;
    this.risks = risks;
  }
}
