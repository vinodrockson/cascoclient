import {VehicleProducer} from './vehicle-producer';

export class VehicleDto {
  id: string;
  plateNumber: string;
  firstRegistrationYear: number;
  purchasePrice: number;
  vehicleProducer: VehicleProducer;
  mileage: bigint;
  previousIndemnity: number;
  annualFee: number;
  monthlyFee: number;
  active: boolean;
  createdOn: Date;
}
