import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {VehicleProducerService} from '../../shared/service/vehicle-producer.service';
import {VehicleProducer} from '../../shared/model/vehicle-producer';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-update-car',
  templateUrl: './update-car.component.html',
  styleUrls: ['../add-car/add-car.component.css']
})
export class UpdateCarComponent implements OnInit {
  riskCoefficientGroup: FormGroup;
  purchasePriceGroup: FormGroup;
  vehicleProducerGroup: FormGroup;
  isStepEditable: boolean;
  errorMessage: string;

  hasError = false;
  showSpinner = true;

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<UpdateCarComponent>, private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: VehicleProducer, private vehicleProducerService: VehicleProducerService) {
  }

  ngOnInit(): void {
    this.isStepEditable = true;

    this.vehicleProducerGroup = this.formBuilder.group({
      vpCtrl: [this.data.name, Validators.required]
    });

    this.purchasePriceGroup = this.formBuilder.group({
      priceCtrl: [this.data.averagePurchasePrice, [Validators.required, Validators.min(1)]]
    });

    this.riskCoefficientGroup = this.formBuilder.group({
      rcCtrl: [this.data.riskCoefficient, [Validators.required]]
    });
  }

  updateCar() {
    const vehicleProducer = new VehicleProducer(this.data.id, this.vehicleProducerGroup.get('vpCtrl').value,
      this.purchasePriceGroup.get('priceCtrl').value, this.riskCoefficientGroup.get('rcCtrl').value, this.data.isActive,
      this.data.createdDate);

    this.vehicleProducerService.updateProducer(vehicleProducer)
      .subscribe(() => {
        this.isStepEditable = false;
        this.showSpinner = false;
        this.onNoClick();
        this.snackBar.open('Car has been updated successfully!', 'Close', {
          duration: 6000,
          panelClass: 'success-message'
        });
        window.location.reload();
      }, error => {
        this.showSpinner = false;
        this.isStepEditable = false;
        this.hasError = true;
        this.errorMessage = error.error.message + error.error.details.toString();
      });
  }

  resetResult() {
    this.hasError = false;
    this.showSpinner = true;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
