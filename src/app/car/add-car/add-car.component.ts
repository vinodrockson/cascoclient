import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {VehicleProducer} from '../../shared/model/vehicle-producer';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VehicleProducerService} from '../../shared/service/vehicle-producer.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})
export class AddCarComponent implements OnInit {
  riskCoefficientGroup: FormGroup;
  purchasePriceGroup: FormGroup;
  vehicleProducerGroup: FormGroup;
  isStepEditable: boolean;
  errorMessage: string;

  hasError = false;
  showSpinner = true;

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<AddCarComponent>, private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public isCarAdded: boolean, private vehicleProducerService: VehicleProducerService) {
  }

  ngOnInit(): void {
    this.isStepEditable = true;

    this.vehicleProducerGroup = this.formBuilder.group({
      vpCtrl: ['', Validators.required]
    });

    this.purchasePriceGroup = this.formBuilder.group({
      priceCtrl: [null, [Validators.required, Validators.min(1)]]
    });

    this.riskCoefficientGroup = this.formBuilder.group({
      rcCtrl: [null, [Validators.required]]
    });
  }

  addCar() {
    const vehicleProducer = new VehicleProducer(null, this.vehicleProducerGroup.get('vpCtrl').value,
      this.purchasePriceGroup.get('priceCtrl').value, this.riskCoefficientGroup.get('rcCtrl').value, true, null);

    this.vehicleProducerService.createProducer(vehicleProducer)
      .subscribe(value => {
        this.isCarAdded = true;
        this.isStepEditable = false;
        this.showSpinner = false;
        this.onNoClick();
        this.snackBar.open('Car has been added successfully!', 'Close', {
          duration: 6000,
          panelClass: 'success-message'
        });
        window.location.reload();
      }, error => {
        this.showSpinner = false;
        this.isStepEditable = false;
        this.hasError = true;
        this.errorMessage = error.error.message + error.error.details.toString();
      });
  }

  resetResult() {
    this.isCarAdded = false;
    this.hasError = false;
    this.showSpinner = true;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
