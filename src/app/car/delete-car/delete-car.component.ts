import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {VehicleProducerService} from '../../shared/service/vehicle-producer.service';

@Component({
  selector: 'app-delete-car',
  templateUrl: './delete-car.component.html',
  styleUrls: ['../add-car/add-car.component.css']
})
export class DeleteCarComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteCarComponent>, private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public id: string, private vehicleProducerService: VehicleProducerService) {
  }

  ngOnInit(): void {
  }

  deleteCar() {
    this.vehicleProducerService.deleteProducer(this.id)
      .subscribe(value => {
        this.dialogRef.close();
        this.snackBar.open('Car has been deleted successfully!', 'Close', {
          duration: 6000,
          panelClass: 'success-message'
        });
        window.location.reload();
      }, error => {
        this.dialogRef.close();
        this.snackBar.open('Cannot delete car! Please try again later!', 'Close', {
          duration: 6000,
          panelClass: 'snack-error-message'
        });
      });
  }
}
