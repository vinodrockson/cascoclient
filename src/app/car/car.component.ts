import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {merge, Observable} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {MatSort} from '@angular/material/sort';
import {VehicleProducerService} from '../shared/service/vehicle-producer.service';
import {AddCarComponent} from './add-car/add-car.component';
import {MatDialog} from '@angular/material/dialog';
import {UpdateCarComponent} from './update-car/update-car.component';
import {VehicleProducer} from '../shared/model/vehicle-producer';
import {DeleteCarComponent} from './delete-car/delete-car.component';
import {RestoreCarComponent} from './restore-car/restore-car.component';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'createdDate', 'name', 'averagePurchasePrice', 'riskCoefficient', 'isActive', 'actions'];
  data: VehicleProducer[] = [];
  isCarAdded: boolean;

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  displayPageSize = environment.itemsPerPage;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private vehicleProducerService: VehicleProducerService, public dialog: MatDialog) {
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.vehicleProducerService.getAllProducers(this.sort.active, this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.totalElements;
          return data.vehicleProducerList;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return new Observable();
        })
      ).subscribe((data: any) => this.data = data);
  }

  addCar(): void {
    const dialogRef = this.dialog.open(AddCarComponent, {
      width: '400px',
      data: this.isCarAdded
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  updateCar(vehicleProducer: VehicleProducer): void {
    const dialogRef = this.dialog.open(UpdateCarComponent, {
      width: '400px',
      data: vehicleProducer
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteCar(vehicleProducer: VehicleProducer): void {
    const dialogRef = this.dialog.open(DeleteCarComponent, {
      width: '400px',
      data: vehicleProducer.id
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  restoreCar(vehicleProducer: VehicleProducer): void {
    const dialogRef = this.dialog.open(RestoreCarComponent, {
      width: '400px',
      data: vehicleProducer.id
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
