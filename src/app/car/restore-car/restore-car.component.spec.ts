import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestoreCarComponent } from './restore-car.component';

describe('RestoreCarComponent', () => {
  let component: RestoreCarComponent;
  let fixture: ComponentFixture<RestoreCarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestoreCarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestoreCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
