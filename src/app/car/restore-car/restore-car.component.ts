import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {VehicleProducerService} from '../../shared/service/vehicle-producer.service';

@Component({
  selector: 'app-restore-car',
  templateUrl: './restore-car.component.html',
  styleUrls: ['../add-car/add-car.component.css']
})
export class RestoreCarComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RestoreCarComponent>, private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public id: string, private vehicleProducerService: VehicleProducerService) {

  }

  ngOnInit(): void {
  }

  restoreCar() {
    this.vehicleProducerService.restoreProducer(this.id)
      .subscribe(value => {
        this.dialogRef.close();
        this.snackBar.open('Car has been restored successfully!', 'Close', {
          duration: 6000,
          panelClass: 'success-message'
        });
        window.location.reload();
      }, error => {
        this.dialogRef.close();
        this.snackBar.open('Cannot restore car! Please try again later!', 'Close', {
          duration: 6000,
          panelClass: 'snack-error-message'
        });
      });
  }
}
