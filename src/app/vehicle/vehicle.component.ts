import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {VehicleService} from '../shared/service/vehicle.service';
import {merge, Observable} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {VehicleDto} from '../shared/model/vehicle-dto';
import {environment} from '../../environments/environment';
import {MatSort} from '@angular/material/sort';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements AfterViewInit {
  displayedColumns: string[] = ['vehicle.id', 'vehicle.createdDate', 'vehicle.plateNumber', 'vehicle.firstRegistrationYear', 'vehicle.vehicleProducer',
    'vehicle.purchasePrice', 'vehicle.mileage', 'vehicle.previousIndemnity', 'annualFee'];
  data: VehicleDto[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  displayPageSize = environment.itemsPerPage;
  isMonthlyFeeVisible = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private vehicleService: VehicleService) {
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.vehicleService.getAllVehicles(this.sort.active, this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.totalElements;
          return data.vehicleList;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return new Observable();
        })
      ).subscribe((data: any) => this.data = data);
  }


  onToggle(event: MatSlideToggleChange) {
    if (event.checked === true) {
      this.isMonthlyFeeVisible = true;
      this.displayedColumns.push('monthlyFee');
    } else {
      this.isMonthlyFeeVisible = false;
      this.displayedColumns.splice(this.displayedColumns.length - 1, 1);
    }
  }
}
