import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SettingService} from '../../shared/service/setting.service';
import {Setting} from '../../shared/model/setting';
import {RiskComponent} from '../risk.component';
import {VehicleService} from '../../shared/service/vehicle.service';

@Component({
  selector: 'app-add-risk',
  templateUrl: './add-risk.component.html',
  styleUrls: ['./add-risk.component.css']
})
export class AddRiskComponent implements OnInit {
  nameGroup: FormGroup;
  valueGroup: FormGroup;
  activeGroup: FormGroup;
  isStepEditable: boolean;
  errorMessage: string;
  showForm: boolean;

  parameters = [];
  hasError = false;
  showSpinner = true;

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<AddRiskComponent>, private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public isRiskAdded: boolean, private settingService: SettingService,
              private vehicleService: VehicleService) {
  }

  ngOnInit(): void {
    this.isStepEditable = true;

    this.vehicleService.getAllValidVehicleFields(RiskComponent.SETTING_RISK).subscribe((value: any[]) => {
      this.parameters = value;

      if (value.length > 0) {
        this.showForm = true;
      }
    });

    this.nameGroup = this.formBuilder.group({
      nameCtrl: ['', Validators.required]
    });

    this.valueGroup = this.formBuilder.group({
      valueCtrl: [null, [Validators.required, Validators.min(0)]]
    });

    this.activeGroup = this.formBuilder.group({
      activeCtrl: [null, [Validators.required]]
    });
  }

  addRisk() {
    const setting = new Setting(null, RiskComponent.SETTING_RISK, this.nameGroup.get('nameCtrl').value,
      this.valueGroup.get('valueCtrl').value, this.activeGroup.get('activeCtrl').value, null);

    this.settingService.createSetting(setting)
      .subscribe(value => {
        this.isRiskAdded = true;
        this.isStepEditable = false;
        this.showSpinner = false;
        this.onNoClick();
        this.snackBar.open('Risk has been added successfully!', 'Close', {
          duration: 6000,
          panelClass: 'success-message'
        });
        window.location.reload();
      }, error => {
        this.showSpinner = false;
        this.isStepEditable = false;
        this.hasError = true;
        this.errorMessage = error.error.message + error.error.details.toString();
      });
  }

  resetResult() {
    this.isRiskAdded = false;
    this.hasError = false;
    this.showSpinner = true;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
