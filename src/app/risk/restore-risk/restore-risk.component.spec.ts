import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestoreRiskComponent } from './restore-risk.component';

describe('RestoreRiskComponent', () => {
  let component: RestoreRiskComponent;
  let fixture: ComponentFixture<RestoreRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestoreRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestoreRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
