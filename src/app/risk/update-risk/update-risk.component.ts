import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SettingService} from '../../shared/service/setting.service';
import {Setting} from '../../shared/model/setting';

@Component({
  selector: 'app-update-risk',
  templateUrl: './update-risk.component.html',
  styleUrls: ['../add-risk/add-risk.component.css']
})
export class UpdateRiskComponent implements OnInit {
  nameGroup: FormGroup;
  valueGroup: FormGroup;
  activeGroup: FormGroup;
  isStepEditable: boolean;
  errorMessage: string;

  hasError = false;
  showSpinner = true;

  constructor(private formBuilder: FormBuilder, public dialogRef: MatDialogRef<UpdateRiskComponent>, private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: Setting, private settingService: SettingService) {
  }

  ngOnInit(): void {
    this.isStepEditable = true;

    this.nameGroup = this.formBuilder.group({
      nameCtrl: [this.data.name, Validators.required]
    });

    this.valueGroup = this.formBuilder.group({
      valueCtrl: [this.data.value, [Validators.required, Validators.min(0)]]
    });

    this.activeGroup = this.formBuilder.group({
      activeCtrl: [this.data.isActive, [Validators.required]]
    });
  }

  updateRisk() {
    const setting = new Setting(this.data.id, this.data.settingType, this.data.name,
      this.valueGroup.get('valueCtrl').value, this.activeGroup.get('activeCtrl').value, null);

    this.settingService.updateSetting(setting)
      .subscribe(value => {
        this.isStepEditable = false;
        this.showSpinner = false;
        this.onNoClick();
        this.snackBar.open('Risk has been updated successfully!', 'Close', {
          duration: 6000,
          panelClass: 'success-message'
        });
        window.location.reload();
      }, error => {
        this.showSpinner = false;
        this.isStepEditable = false;
        this.hasError = true;
        this.errorMessage = error.error.message + error.error.details.toString();
      });
  }

  resetResult() {
    this.hasError = false;
    this.showSpinner = true;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
