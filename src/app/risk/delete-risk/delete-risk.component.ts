import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SettingService} from '../../shared/service/setting.service';

@Component({
  selector: 'app-delete-risk',
  templateUrl: './delete-risk.component.html',
  styleUrls: ['../add-risk/add-risk.component.css']
})
export class DeleteRiskComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteRiskComponent>, private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public id: string, private settingService: SettingService) {
  }

  ngOnInit(): void {
  }

  deleteRisk() {
    this.settingService.deleteSetting(this.id)
      .subscribe(value => {
        this.dialogRef.close();
        this.snackBar.open('Risk has been deleted successfully!', 'Close', {
          duration: 6000,
          panelClass: 'success-message'
        });
        window.location.reload();
      }, error => {
        this.dialogRef.close();
        this.snackBar.open('Cannot delete risk! Please try again later!', 'Close', {
          duration: 6000,
          panelClass: 'snack-error-message'
        });
      });
  }
}
