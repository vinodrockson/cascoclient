import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {merge, Observable} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {MatSort} from '@angular/material/sort';
import {AddRiskComponent} from './add-risk/add-risk.component';
import {MatDialog} from '@angular/material/dialog';
import {UpdateRiskComponent} from './update-risk/update-risk.component';
import {DeleteRiskComponent} from './delete-risk/delete-risk.component';
import {RestoreRiskComponent} from './restore-risk/restore-risk.component';
import {Setting} from '../shared/model/setting';
import {SettingService} from '../shared/service/setting.service';

@Component({
  selector: 'app-risk',
  templateUrl: './risk.component.html',
  styleUrls: ['./risk.component.css']
})
export class RiskComponent implements AfterViewInit {
  public static SETTING_RISK = 'RISK_COEFFICIENT';

  displayedColumns: string[] = ['id', 'createdDate', 'name', 'value', 'isActive', 'actions'];
  data: Setting[] = [];
  isRiskAdded: boolean;

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  displayPageSize = environment.itemsPerPage;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private settingService: SettingService, public dialog: MatDialog) {
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.settingService.getAllSettingsByType(RiskComponent.SETTING_RISK, this.sort.active, this.sort.direction,
            this.paginator.pageIndex);
        }),
        map(data => {
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.totalElements;
          return data.settingList;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
          return new Observable();
        })
      ).subscribe((data: any) => this.data = data);
  }

  addRisk(): void {
    const dialogRef = this.dialog.open(AddRiskComponent, {
      width: '400px',
      data: this.isRiskAdded
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  updateRisk(setting: Setting): void {
    const dialogRef = this.dialog.open(UpdateRiskComponent, {
      width: '400px',
      data: setting
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  deleteRisk(setting: Setting): void {
    const dialogRef = this.dialog.open(DeleteRiskComponent, {
      width: '400px',
      data: setting.id
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  restoreRisk(setting: Setting): void {
    const dialogRef = this.dialog.open(RestoreRiskComponent, {
      width: '400px',
      data: setting.id
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
