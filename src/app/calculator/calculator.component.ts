import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VehicleProducerService} from '../shared/service/vehicle-producer.service';
import {Vehicle} from '../shared/model/vehicle';
import {InsuranceFee} from '../shared/model/insurance-fee';
import {InsuranceFeeService} from '../shared/service/insurance-fee.service';
import {VehicleProducer} from '../shared/model/vehicle-producer';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private vehicleProducerService: VehicleProducerService,
              private insuranceFeeService: InsuranceFeeService) {
  }

  vehicleProducerGroup: FormGroup;
  plateNumberGroup: FormGroup;
  regYearGroup: FormGroup;
  purchasePriceGroup: FormGroup;
  mileageGroup: FormGroup;
  previousIndemnityGroup: FormGroup;
  otherOptionsGroup: FormGroup;
  insuranceFee: InsuranceFee;
  errorMessage: string;
  isStepEditable: boolean;
  showCalculator: boolean;

  vehicleProducers = [];
  isFeeCalculated = false;
  hasError = false;
  showSpinner = true;
  showMonthlyFee = false;
  showPreviousIndemnity = false;

  ngOnInit() {
    this.isStepEditable = true;

    this.vehicleProducerService.getActiveProducers().subscribe((data: any[]) => {
      this.vehicleProducers = data;

      if (this.vehicleProducers.length > 0) {
        this.showCalculator = true;
      }
    });

    this.vehicleProducerGroup = this.formBuilder.group({
      vpCtrl: ['', Validators.required]
    });

    this.plateNumberGroup = this.formBuilder.group({
      pnCtrl: ['', [Validators.required, Validators.maxLength(6), Validators.pattern('[0-9]{3}[A-Za-z]{3}')]]
    });

    this.regYearGroup = this.formBuilder.group({
      yearCtrl: [null, [Validators.required, Validators.min(1000), Validators.max(new Date().getFullYear())]]
    });

    this.purchasePriceGroup = this.formBuilder.group({
      priceCtrl: [null, [Validators.required, Validators.min(1)]]
    });

    this.mileageGroup = this.formBuilder.group({
      mileageCtrl: [null, [Validators.required, Validators.min(0)]]
    });

    this.previousIndemnityGroup = this.formBuilder.group({
      piCtrl: [null, [Validators.required, Validators.min(0)]]
    });

    this.otherOptionsGroup = this.formBuilder.group({
      calMonthlyFee: '',
      addPreviousIndemnity: ''
    });
  }

  calculate() {
    const vehicleProducer = new VehicleProducer(this.vehicleProducerGroup.get('vpCtrl').value, null, null,
      null, true, null);
    const vehicle = new Vehicle(null, this.plateNumberGroup.get('pnCtrl').value, this.regYearGroup.get('yearCtrl').value,
      this.purchasePriceGroup.get('priceCtrl').value, vehicleProducer, this.mileageGroup.get('mileageCtrl').value,
      this.previousIndemnityGroup.get('piCtrl').value, true);
    const addPreviousIndemnity = this.otherOptionsGroup.get('addPreviousIndemnity').value === true;

    console.log(vehicle);
    console.log(addPreviousIndemnity);
    this.insuranceFeeService.postInsuranceFee(vehicle, addPreviousIndemnity)
      .subscribe(value => {
        this.isFeeCalculated = true;
        this.isStepEditable = false;
        this.showSpinner = false;
        this.insuranceFee = new InsuranceFee(value.id, value.annualFee, value.monthlyFee, value.vehicleProducer,
          value.firstRegistrationYear, value.mileage, value.previousIndemnity, value.feeCalculatedDate, value.risks);
        this.showMonthlyFee = this.otherOptionsGroup.get('calMonthlyFee').value === true ? true : this.showMonthlyFee;
      }, error => {
        this.showSpinner = false;
        this.isStepEditable = false;
        this.hasError = true;
        this.errorMessage = error.error.message + error.error.details.toString();
      });
  }

  resetResult() {
    this.isFeeCalculated = false;
    this.hasError = false;
    this.showSpinner = true;
    this.showMonthlyFee = false;
  }

  previousIndemnityCheck() {
    if (this.previousIndemnityGroup.get('piCtrl').value > 0) {
      this.showPreviousIndemnity = true;
    }
  }
}
